include(${CMAKE_CURRENT_LIST_DIR}/Details/EnablePrecompiledHeadersUsageVerification.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Details/AddPrecompiledHeader.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Details/AddPrecompiledHeaderToTarget.cmake)
