macro(add_precompiled_header _targetName _input)
    get_filename_component(_name ${_input} NAME)
    set(_source "${CMAKE_CURRENT_SOURCE_DIR}/${_input}")
    set(_outdir "${CMAKE_CURRENT_BINARY_DIR}/${_targetName}")
    set(_output "${_outdir}/${_name}.gch")

    file(MAKE_DIRECTORY ${_outdir})

    get_directory_property(_directory_flags INCLUDE_DIRECTORIES)
    foreach(item ${_directory_flags})
      list(APPEND _compiler_FLAGS "-I${item}")
    endforeach(item)

    get_directory_property(_directory_compile_options COMPILE_OPTIONS)
    foreach(item ${_directory_compile_options})
      list(APPEND _compiler_FLAGS "${item}")
    endforeach(item)
    get_directory_property(_directory_compile_defs COMPILE_DEFINITIONS)
    foreach(item ${_directory_compile_defs})
      list(APPEND _compiler_FLAGS "-D${item}")
    endforeach(item)

    set(_COMPILER_OPTS_LIST "${_compiler_FLAGS} -g -x c++-header -o ${_output} ${_source}")
    separate_arguments(_COMPILER_OPTS_LIST)
    separate_arguments(_compiler_FLAGS)

    if(${enable_precompiled_headers_usage_verification})
      add_custom_command(
          OUTPUT ${_output} ${_outdir}/${_name}
          COMMAND ${CMAKE_CXX_COMPILER} ${_COMPILER_OPTS_LIST}
          COMMAND echo "#include <${_source}>" > ${_outdir}/${_name}
          COMMAND echo "#error percompiled header for ${CMAKE_CURRENT_SOURCE_DIR}/${_input} not in use" >> ${_outdir}/${_name}
          DEPENDS ${_source}
          IMPLICIT_DEPENDS CXX ${_source}
          VERBATIM)
    else(${enable_precompiled_headers_usage_verification})
      add_custom_command(
          OUTPUT ${_output}
          COMMAND ${CMAKE_CXX_COMPILER} ${_COMPILER_OPTS_LIST}
          COMMAND rm -rf ${_outdir}/${_name}
          DEPENDS ${_source}
          IMPLICIT_DEPENDS CXX ${_source}
          VERBATIM)
    endif(${enable_precompiled_headers_usage_verification})


    add_custom_target(${_targetName} DEPENDS ${_output})
    set(${_targetName}_INCLUDE_DIRECTORIES ${_outdir})
endmacro(add_precompiled_header)
