macro(add_precompiled_header_to_target _targetName)
    if(${ARGC} LESS 1)
       message(FATAL_ERROR "Invalid use of macro add_precompiled_header_to_target! Should be: add_precompiled_header_to_target(targetName gchTargetName [gchTargetName1... ])")
    endif(${ARGC} LESS 1)
    SET(ARGN_COPY ${ARGN})
    foreach(_gchTarget IN LISTS ARGN_COPY)
       add_dependencies(${_targetName} ${_gchTarget})
       set(_gchIncludeDirectories ${_gchTarget}_INCLUDE_DIRECTORIES)
       target_include_directories(${_targetName} BEFORE PUBLIC "${${_gchIncludeDirectories}}")
    endforeach()
endmacro(add_precompiled_header_to_target)
